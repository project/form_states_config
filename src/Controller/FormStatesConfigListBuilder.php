<?php
namespace Drupal\form_states_config\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of form states configurations.
 */
class FormStatesConfigListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['form_id'] = $this->t('Form ID');
    $header['operations'] = $this->t('Operations');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['form_id'] = $entity->getFscFormId();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    $operations = parent::getDefaultOperations($entity);
    $operations['edit'] = [
      'title' => $this->t('Edit'),
      'weight' => 0,
      'url' => Url::fromRoute('entity.form_states_config.edit_form', ['form_states_config' => $entity->id()]),
    ];
    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'weight' => 0,
      'url' => Url::fromRoute('entity.form_states_config.delete_form', ['form_states_config' => $entity->id()]),
    ];

    return $operations;
  }

}
