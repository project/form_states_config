<?php

namespace Drupal\form_states_config\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Form controller for the Form States Configuration add/edit forms.
 */
class FormStatesConfigForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    $form['fsc_form_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form ID'),
      '#default_value' => $entity->getFscFormId(),
      '#required' => TRUE,
      '#disabled' => !$entity->isNew(),
      '#description' => $this->t('ID of a form to be altered to add form states.'),
    ];

    $arguments = [
      ':form_states' => Url::fromUri('https://www.drupal.org/docs/drupal-apis/form-api/conditional-form-fields')->toString(),
      '@form_states' => 'form states',
    ];

    $form['fsc_form_states'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Form States'),
      '#default_value' => $entity->getFscFormStates(),
      '#required' => TRUE,
      '#rows' => 10,
      '#description' => $this->t('Form states conditions in yaml format to be added to a form. Read more about using <a href=":form_states">@form_states</a>.', $arguments),
    ];

    $form['#validate'][] = '::validateFormStatesConfig';

    return $form;
  }

  /**
   * Validate submission of the form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validateFormStatesConfig(array $form, FormStateInterface $form_state): void {
    if ($this->entity->isNew()) {
      $form_states_config_storage = \Drupal::entityTypeManager()->getStorage('form_states_config');

      /** @var \Drupal\form_states_config\Entity\FormStatesConfig $form_states_config */
      $form_states_config = $form_states_config_storage->load($form_state->getValue('fsc_form_id'));
      if (!empty($form_states_config)) {
        $form_state->setErrorByName('fsc_form_id', $this->t('There is already existing form states config for form %form_id.', ['%form_id' => $form_state->getValue('fsc_form_id')]));
      }
    }
    // @TODO: move validation to constraints.
    $fsc_form_states = $form_state->getValue('fsc_form_states');
    try {
      Yaml::parse($fsc_form_states);
    }
    catch (ParseException $exception) {
      $form_state->setErrorByName('fsc_form_states', $this->t('Entered form states conditions have incorrect yaml format.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $entity->setFscFormId($form_state->getValue('fsc_form_id'))
      ->setFscFormStates($form_state->getValue('fsc_form_states'));

    $status = $entity->save();

    if (!empty($status)) {
      $this->messenger()->addMessage($this->t('Form states configuration %label has been saved.', ['%label' => $entity->label()]));
    }
    else {
      $this->messenger()->addMessage($this->t('There was a problem saving the form states configuration.'), 'error');
    }

    $form_state->setRedirect('entity.form_states_config.collection');
  }

}
