<?php

namespace Drupal\form_states_config\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Form States Configuration entity.
 *
 * @ConfigEntityType(
 *   id = "form_states_config",
 *   label = "Form States Configuration",
 *   handlers = {
 *     "list_builder" = "Drupal\form_states_config\Controller\FormStatesConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\form_states_config\Form\FormStatesConfigForm",
 *       "edit" = "Drupal\form_states_config\Form\FormStatesConfigForm",
 *       "delete" = "Drupal\form_states_config\Form\FormStatesConfigDeleteForm"
 *     },
 *   },
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "fsc_form_id",
 *     "label" = "fsc_form_id"
 *   },
 *   links = {
 *     "collection" = "/admin/structure/form-states-config",
 *     "add-form" = "/admin/structure/form-state-config/add",
 *     "edit-form" = "/admin/structure/form-state-config/{form_state_config}/edit",
 *     "delete-form" = "/admin/structure/form-state-config/{form_state_config}/delete"
 *   },
 *   config_export = {
 *     "fsc_form_id",
 *     "fsc_form_states"
 *   }
 * )
 */
class FormStatesConfig extends ConfigEntityBase {

  /**
   * The form ID.
   *
   * @var string
   */
  protected $fsc_form_id;

  /**
   * The form state.
   *
   * @var string
   */
  protected $fsc_form_states;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->getFscFormId();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getFscFormId();
  }

  /**
   * Get the form ID.
   *
   * @return string
   *   The form ID.
   */
  public function getFscFormId() {
    return $this->fsc_form_id;
  }

  /**
   * Set the form ID.
   *
   * @param string $fsc_form_id
   *   The form ID.
   *
   * @return $this
   *   The current instance.
   */
  public function setFscFormId($fsc_form_id) {
    $this->fsc_form_id = $fsc_form_id;
    return $this;
  }

  /**
   * Get the form state.
   *
   * @return string
   *   The form state.
   */
  public function getFscFormStates() {
    return $this->fsc_form_states;
  }

  /**
   * Set the form state.
   *
   * @param array $fsc_form_states
   *   The form state.
   *
   * @return $this
   *   The current instance.
   */
  public function setFscFormStates($fsc_form_states) {
    $this->fsc_form_states = $fsc_form_states;
    return $this;
  }

}
